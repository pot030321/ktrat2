﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace baikiemtra.Models
{
    public class Logs
    {
        [Key]
        public int logId { get; set; }
        public string LoginDate { get; set; }
        public string LoginTime { get; set; }
        [JsonIgnore]
        public List<Transactions>? transactions { get; set; }
        [JsonIgnore]
        public report?  report { get; set; }
    }
}
