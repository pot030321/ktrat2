﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace baikiemtra.Models
{
    public class report
    {
        [Key]
        public int ReportID { get; set; }
        public string ReportName { get; set; }
        public string ReportDate { get; set; }
        [JsonIgnore]
        public List<Transactions>? Transactions { get; set; }
        [JsonIgnore]
        public List<Logs>? Logs { get; set; }
        [JsonIgnore]
        public List<Accounts>? Accounts { get; set; }
        
    }
}
