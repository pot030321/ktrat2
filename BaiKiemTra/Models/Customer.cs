﻿using baikiemtra.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Transactions;
using System.Web;

namespace BaiKiemTra.Models
{
    public class Customer
    {
        [Key]
        public int CustomerID { set; get; }
        public string FisrtName { get; set; }

        public string LastName { get; set; }
        public string ContractAndAddress { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        [JsonIgnore]
        public Transactions? Transactions { get; set; }
        [JsonIgnore]
        public Accounts? Accounts { get; set; }

    }
}