﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using System.Transactions;

namespace baikiemtra.Models
{
    public class Employees
    {
        [Key]
        public int EmployeeID { get; set; }
        public string FirstName { get; set; }
        public string ContractAndAddress { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        [JsonIgnore]
        public Transactions? Transactions { get; set; }
    }
}
