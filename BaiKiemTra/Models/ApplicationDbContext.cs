﻿using baikiemtra.Models;
using BaiKiemTra.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace WebThu2.Models
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext>
        options) : base(options)
        {
        }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Accounts> Accounts { get; set; }
        public DbSet<Employees> Employees { get; set; }
        public DbSet<Logs> Logs { get; set; }
        public DbSet<report> Reports { get; set; }
        public DbSet<Transactions> Transactions { get; set; }
    }
}
